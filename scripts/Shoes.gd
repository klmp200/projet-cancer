extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var min_to_survive = 2
var cost = 5

func _ready():
	efficiencies = {
			"Cables" : 2,
			"Nuclear_addon" : 2,
			"Top_pipe" : 1,
			"Side_pipe" : 1,
			"Filter" : 3,
			"Window" : 1
	}

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
