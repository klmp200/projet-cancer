extends "Tool.gd"

var min_to_survive = 2

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var cost = 7

func _ready():
	efficiencies = {
			"Cables" : 1,
			"Nuclear_addon" : 3,
			"Top_pipe" : 5,
			"Side_pipe" : 5,
			"Filter" : 2,
			"Window" : 7
	}
	
	# Called every time the node is added to the scene.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
