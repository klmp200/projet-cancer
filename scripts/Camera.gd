extends Camera

var angle
var dist
var scene_manager

func _ready():
	dist = 12
	self.look_at_from_position(Vector3(dist, self.translation.y, 0), Vector3(0,3,0),
		Vector3(0,1,0))
	angle = 0
	scene_manager = get_tree().get_root().get_node("SceneManager")

func _process(delta):
	if Input.is_key_pressed(KEY_Q) or Input.is_key_pressed(KEY_D):
		if Input.is_key_pressed(KEY_Q):
			angle += delta
		elif Input.is_key_pressed(KEY_D):
			angle -= delta
		var reactorPos = get_node("../Reactor").translation
		var newX = dist * cos(angle)
		var newZ = dist * sin(angle)
		self.look_at_from_position(Vector3(newX, self.translation.y, newZ), Vector3(0,3,0),
		Vector3(0,1,0))
		
func part_projected_on(mouse_pos):
	var ray_length = 10000
	var space_state = self.get_world().direct_space_state
	var from = self.project_ray_origin(mouse_pos)
	var to = from + self.project_ray_normal(mouse_pos) * ray_length
	var ray_cast_result = space_state.intersect_ray(from, to)
	if ray_cast_result.empty():
		return null
	else:
		return ray_cast_result.collider.get_parent()
	
func _physics_process(delta):
	var part_under_mouse = self.part_projected_on(get_viewport().get_mouse_position())
	var material = null
	if part_under_mouse != null:
		part_under_mouse.highlight()

	if get_node("../Reactor"):
		for child in self.get_node("../Reactor").get_children():
			if child != part_under_mouse:
				child.unhighlight()
		

