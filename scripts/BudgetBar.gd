extends ProgressBar

var scene_manager


func _ready():
	if get_tree().get_root().get_node("SceneManager") != null:
		scene_manager = get_tree().get_root().get_node("SceneManager")
	else:
		scene_manager = null

func _process(delta):
	if scene_manager != null and scene_manager.player.budget <= 100:
		update_value()
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func reset():
	if scene_manager != null and scene_manager.player.budget <= 80:
		scene_manager.player.budget += 20
		update_value()
		
func update_value():
	self.value = scene_manager.player.budget