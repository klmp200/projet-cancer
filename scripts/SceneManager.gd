extends Node

const Player = preload("res://scripts/Player.gd")
const GameStatus = preload("res://scripts/GameStatus.gd")
const Reactor = preload("res://scenes/Reactor.tscn")
const Tool = preload("res://scripts/Tool.gd")
const Menu = preload("res://scenes/Menu.tscn")
const ReactorScene = preload("res://scenes/ReactorScene.tscn")
const Pause = preload("res://scenes/Pause.tscn")
const CancerDeath = preload("res://scenes/CancerDeath.tscn")
const ExplosionDeath = preload("res://scenes/ExplosionDeath.tscn")
const Win = preload("res://scenes/Win.tscn")

var reactor_number = 5
var preview_number = 3
var pause_number = 3
var round_number = 10

var player = Player.new()
var game_status = GameStatus.new()

var scenes = []
var scenes_fifo = []
var active_scene = null

var menu = Menu.instance()
var reactor_scene = ReactorScene.instance()
var pause = Pause.instance()


func _ready():
	assert(reactor_number > preview_number)
	for i in range(0, reactor_number):
		var reactor = Reactor.instance()
		reactor.init(i)
		scenes.append([1, reactor, true])
	for i in range(0, pause_number):
		scenes.append([2, pause, true])
	scenes = shuffle_list(scenes)
	active_scene = [0, menu, false]
	for i in range(0, preview_number):
		scenes_fifo.push_front(scenes.pop_front())
	self.add_child(menu)

func get_active_scene():
	return active_scene

func get_scene_preview():
	return scenes_fifo

func next_scene(scene=null, type=0, shuffle=true):
	var tmp_scene = active_scene
	hide_current_scene()
	if scene != null:
		active_scene = [type, scene, shuffle]
	else:
		active_scene = scenes_fifo.pop_back()
	scenes = shuffle_list(scenes)
	scenes_fifo.push_front(scenes.pop_front())
	if tmp_scene[2]:
		scenes.append(tmp_scene)
	game_status.round_number += 1
	if (game_status.round_number > round_number):
		active_scene = [3, "victory", false]
	change_scene()

func shuffle_list(list):
	var shuffled_list = []
	var index_list = range(list.size())
	for i in range(list.size()):
		randomize()
		var x = randi() % index_list.size()
		shuffled_list.append(list[x])
		index_list.remove(x)
		list.remove(x)
	return shuffled_list

func launch_reactor():
	reactor_scene.add_child(active_scene[1])
	add_child(reactor_scene)
	reactor_scene.reset()


func launch_menu():
	add_child(menu)

func launch_pause():
	add_child(pause)
	pause.reset()

#0 : menu, 1 : reactor, 2 : pause
func change_scene():
	var scene_type = active_scene[0]
	if scene_type == 0:
		self.launch_menu()
	elif scene_type == 1:
		self.launch_reactor()
	elif scene_type == 2:
		self.launch_pause()
	elif scene_type == 3:
		var message = active_scene[1]
		if message == "victory":
			add_child(Win.instance())
		elif message == "explosion":
			add_child(ExplosionDeath.instance())
		elif message == "cancer":
			add_child(CancerDeath.instance())

func hide_current_scene():
	var type = active_scene[0]
	if type == 0:
		remove_child(menu)
	elif type == 1:
		if reactor_scene != null:
			reactor_scene.remove_child(active_scene[1])
			remove_child(reactor_scene)
	elif type == 2:
		remove_child(pause)
	elif type == 3:
		pass

