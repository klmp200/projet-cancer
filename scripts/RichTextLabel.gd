extends RichTextLabel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func reset():
	var scene_manager = get_tree().get_root().get_node("SceneManager")
	if scene_manager != null:
		text = str(scene_manager.active_scene[1].label)
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
