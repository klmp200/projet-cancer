extends Node

var label
# class member variables go here, for example:
# var a = 2
# var b = "textvar"
func init(label=0):
	self.label = label
	
func _ready():
	pass
	# Called every time the node is added to the scene.
	# Initialization here
func reset():
	var damaged = false
	for part in self.get_children():
		part.damage()
		if part.is_damaged:
			damaged = true
	if not damaged:
		self.reset()

func _process(delta):
	var broken_children = 0
	for part in self.get_children():
		broken_children += 1 if part.is_broken else 0
	if broken_children >= 3:
		get_tree().get_root().get_node("SceneManager").next_scene("explosion", 3, false)
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
