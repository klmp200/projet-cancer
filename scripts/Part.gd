extends MeshInstance

var hp = 100
var is_damaged
var is_broken
var damaged_probability
var check_damaged_time = 3
var Smoke = preload("res://scenes/Smoke.tscn")
var smoke
var original_color = get_surface_material(0).albedo_color

func _ready():
	is_damaged = false
	is_broken = false
	damaged_probability = randi() % 80 + 1
	smoke = Smoke.instance()
	self.add_child(smoke)
	smoke.emitting = false

	
func damage():
	if not is_damaged:
		var rand_damage = randi()%100 + 1
		is_damaged = rand_damage < damaged_probability
			
func _process(delta):
	if is_damaged:
		hp -= delta * 10
		if smoke != null:
			smoke.emitting = true
		if hp <= 0:
			is_damaged = false
			is_broken = true
			original_color = Color(1,0,0)
	else:
		if smoke != null:
			smoke.emitting = false

func repair(efficiency):
	if not is_broken:
		if (randi()%100 + 1 <= efficiency * 10):
			is_damaged = false
		hp += efficiency if hp + efficiency <= 100 else 0

func highlight():
	self.get_surface_material(0).albedo_color = Color(original_color.r + 1, original_color.g + 1, original_color.b + 1)

func unhighlight():
	self.get_surface_material(0).albedo_color = original_color
