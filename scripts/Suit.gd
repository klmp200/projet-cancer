extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var min_to_survive = 1
var cost = 20

func _ready():
	efficiencies = {
			"Cables" : 2,
			"Nuclear_addon" : 1,
			"Top_pipe" : 0,
			"Side_pipe" : 1,
			"Filter" : 3,
			"Window" : 2
	}

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
