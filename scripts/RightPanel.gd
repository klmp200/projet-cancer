extends Panel
var dragged_object
var is_reactor_room

var magnet_list = {
		"x_multiplier" : 120,
		"y_multiplier" : 140,
		"x_offset" : 80,
		"y_offset" : 300,
		"rows" : 2
}

var clothes = []
var scene_manager

func reset():
	if scene_manager != null:
		clothes = []
		var i = 0
		var j = 8
		for item in scene_manager.player.supply.keys(): 
			if item.get_parent() != null:
				item.get_parent().remove_child(item)
			self.add_child(item)
			if item.name in ["Suit", "Glove", "Shoes"]:
				item.init(grid_pos(j), scene_manager)
				clothes.append(item)
				j += 1
			else:
				item.init(grid_pos(i), scene_manager)
				i += 1

func _ready():
	
	is_reactor_room = (get_parent().name == "Camera")
	
	scene_manager = get_tree().get_root().get_node("SceneManager")
	reset()


func grid_pos(i):
	if magnet_list["rows"] == 0:
		return null
	else:
		var y = i / magnet_list["rows"]
		var x = i % magnet_list["rows"]
		return (Vector2(magnet_list["x_offset"] + x * magnet_list["x_multiplier"], 
			magnet_list["y_offset"] + y * magnet_list["y_multiplier"]))
			
func _process(delta):
	if scene_manager != null and is_reactor_room:
		var mul = 0
		for item in clothes:
			if scene_manager.player.supply[item] < item.min_to_survive:
				mul += item.min_to_survive - scene_manager.player.supply[item]
		scene_manager.player.cancer += mul * delta
		if scene_manager.player.cancer >= 100:
			scene_manager.next_scene("cancer", 3, true)
