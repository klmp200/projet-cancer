extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	efficiencies = {
			"Cables" : 5,
			"Nuclear_addon" : 5,
			"Top_pipe" : 3,
			"Side_pipe" : 3,
			"Filter" : 2,
			"Window" : 3
	}
	# Called every time the node is added to the scene.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
