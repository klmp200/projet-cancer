extends GridContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const ButtonRes = preload("res://scenes/BuyButton.tscn")
var scene_manager

func _ready():
	scene_manager = get_tree().get_root().get_node("SceneManager")
	if scene_manager != null:
		for item in scene_manager.player.supply.keys():
			var button = ButtonRes.instance()
			add_child(button)
			button.init(item)
			#add_child(item)
	# Called every time the node is added to the scene.
	# Initialization here


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
