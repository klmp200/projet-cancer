extends ProgressBar

var sceneTime
var elapsedTime

func _ready():
	sceneTime = 10
	elapsedTime = 0

func _process(delta):
	elapsedTime += delta
	self.value = (elapsedTime * 100)/sceneTime
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass